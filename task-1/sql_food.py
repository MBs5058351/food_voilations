import pandas as pd
import sqlite3

conn = sqlite3.connect('food_violations.db')
print ("Database co0nnected successfully");


""" 
Logic to List the distinctive businesses that have had at least 1 violation ordered alphabetically

"""
#selecting distinct bussiness with atlest one violation sql query

sql = "SELECT \
    DISTINCT\
    INSPECTIONS.facility_name as name, \
    INSPECTIONS.facility_address as address, \
    INSPECTIONS.facility_zip as zip_code, \
    INSPECTIONS.facility_city as city\
    FROM INSPECTIONS\
    INNER JOIN VIOLATIONS ON INSPECTIONS.serial_number=VIOLATIONS.serial_number ORDER by name ASC;"

cursor = conn.execute(sql)

pdlist=[]
#appending to a list inside the for 
for row in cursor:
   templist=[]
   print ("Name = ", row[0])
   print ("address = ", row[1])
   print ("zip_code = ", row[2])
   print ("city = ", row[3], "\n") 
   templist.append(row[0])
   templist.append(row[1])
   templist.append(row[2])
   templist.append(row[3])
   pdlist.append(templist)
print ("Operation done successfully");

clos=["Name","Address","zip_code","city"]
df=pd.DataFrame(pdlist,columns=clos)
#print(df.head)


""" 

writing name, address, zip code and city into a new database table called
“Previous Violations table in new DB previous Violations

""""
try:
	new_db="Previous_Violations.db"
	table_name="Previous_Violations"
	#creating the conncetion within the food_violations.db
	conn2=sqlite3.connect(new_db)
	#table,name connections
	df.to_sql(new_db,con=conn2)
	conn2.close()

except Exception as e:
	print(e)
	conn2.close()


""" Print count of the violations for each business that has at least 1 violation to the console
along with their name ordered by the number of violations 
 """


try:
	vsql = "select INSPECTIONS.facility_name as name,COUNT(VIOLATIONS.serial_number) as violation_count  from INSPECTIONS INNER JOIN VIOLATIONS ON INSPECTIONS.serial_number=VIOLATIONS.serial_number GROUP BY name ORDER BY violation_count ASC;"
	cursor = conn.execute(vsql)

	for row in cursor:
		print("Name :",row[0]+"  Voilation_count :",row[1])
		print()
	conn.close
except Exception as e:
	print(e)
	conn.close

