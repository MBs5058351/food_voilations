#select distinct violation_code , violation_description,count(violation_code)from violations group by violation_code;
import pandas as pd
import sqlite3 as sq

try:
	sheet_name="Violations Types"
	file_name="ViolationTypes.xlsx"
	conn = sq.connect('food_violations.db')
	print ("Database co0nnected successfully");

	sql="select distinct violation_code , violation_description,count(violation_code) from violations group by violation_code;"
	cursor = conn.execute(sql)
	pdlist=[]
	for row in cursor:
		templist=[]
		print("code = ", row[0])
		print("description = ", row[1])
		print("Count = ", row[2])

		templist.append(row[0])
		templist.append(row[1])
		templist.append(row[2])
		pdlist.append(templist)
	clos=["code","description","count"]
	df=pd.DataFrame(pdlist,columns=clos)
	total=df["count"].sum()

	df2=pd.DataFrame([["Total-Violations",total]],columns=["Total-Violations","sum"])
	print(df2)
	try:
		# Create a Pandas Excel writer using XlsxWriter as the engine.
		writer = pd.ExcelWriter(file_name, engine='xlsxwriter')

		# Convert the dataframe to an XlsxWriter Excel object.
		df.to_excel(writer, sheet_name=sheet_name, index=False)
		
		df2.to_excel(writer, sheet_name=sheet_name,startcol=1,startrow=117,header=False, index=False)

		# Close the Pandas Excel writer and output the Excel file.
		writer.save()
	except Exception as e:
		print(e)
	#print("Total",count)
	conn.close()
except Exception as e:
	print(e)
	conn.close()
